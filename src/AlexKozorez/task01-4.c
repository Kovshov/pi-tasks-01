#include <stdio.h>
#include <math.h>
#include <string.h>

int mod(int i, int k){
	return i % k;
}

int main(){
	
	char str[256] = {0};

	printf("Enter a string with numbers only:\n");
	fgets(str,256,stdin);

	int size = strlen(str)-1, first = mod(size,3), another = size+1-first;

	str[size]='\0';

	for(int i=0;i<first;++i)
		printf("%c",str[i]);

	if(first!=0) printf(" ");
	
	for(int i=0;i<another;++i)
		(mod(i+1,3) == 0) ? printf("%c ",str[i+first]) : printf("%c",str[i+first]);
	
	return 0;

}