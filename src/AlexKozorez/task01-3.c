#include <stdio.h>
#include <math.h>
#include <string.h>

int main(){
	
	char str[256] = {0};

	printf("Enter a string:\n");
	fgets(str,256,stdin);

	int k = 0, result = 0, current = 0, size = strlen(str)-1;

	str[size]='\0';
	
	for(int i=0;i<=size;++i){
		if(str[i]-'0' >= 0 &&  str[i]-'9' <= 9){
			current = current*10+(int)(str[i]-'0') ;
		}else{
			result += current;
			current = 0;
		}
	}

	printf("Sum of string \"Abrakadabra\" is : %d", current !=0 ? result + current : result);

	return 0;	
}